// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.Messaging;
using CommunityToolkit.Mvvm.Messaging.Messages;
using ImapSync.Helpers.Objects;
using ImapSync.Models;
using Wpf.Ui.Controls;

namespace ImapSync.ViewModels.Mail;

public partial class ViewAccountModel : ObservableObject, INavigationAware, IRecipient<ValueChangedMessage<MailInfo>>
{
    [ObservableProperty]
    private RangeObservableCollection<MenuItem?> _contextMenuViewAccount = new();

    [ObservableProperty]
    private MailInfo _selectedMailInfo;

    public ViewAccountModel()
    {
        InitializeContextMenu();
    }

    public void OnNavigatedFrom()
    {
        WeakReferenceMessenger.Default.Unregister<ValueChangedMessage<MailInfo>>(this);
    }

    public void OnNavigatedTo()
    {
        if (!WeakReferenceMessenger.Default.IsRegistered<ValueChangedMessage<MailInfo>>(this))
        {
            WeakReferenceMessenger.Default.Register(this);
        }
    }

    public void Receive(ValueChangedMessage<MailInfo> value)
    {
        SelectedMailInfo = value.Value;
    }

    private void InitializeContextMenu()
    {
        ContextMenuViewAccount.AddRange(new List<MenuItem?>()
        {
            new MenuItem
            {
                Header = "Get EML's",
                Tag = "get_emls",
                Icon = new SymbolIcon() { Symbol = SymbolRegular.ArrowDownload24, Filled = true },
                FontSize = 16,
                Command = null,
            },
            new MenuItem
            {
                Header = "View EML's",
                Tag = "view_emls",
                Icon = new SymbolIcon() { Symbol = SymbolRegular.Eye24, Filled = true },
                FontSize = 16,
                Command = null,
            },
            null,
            new MenuItem
            {
                Header = "Make PST",
                Tag = "make_pst",
                Icon = new SymbolIcon() { Symbol = SymbolRegular.MailInboxAdd24, Filled = true },
                FontSize = 16,
                Command = null,
            },
        });
    }
}
