// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

using ImapSync.Helpers.Objects;
using Wpf.Ui.Controls;

namespace ImapSync.ViewModels;

public partial class MainWindowViewModel : ObservableObject
{
    private bool _isInitialized = false;

    [ObservableProperty]
    private string _applicationTitle = String.Empty;

    [ObservableProperty]
    private RangeObservableCollection<NavigationViewItem> _navigationItems = new();

    [ObservableProperty]
    private RangeObservableCollection<NavigationViewItem> _navigationFooter = new();

    [ObservableProperty]
    private RangeObservableCollection<MenuItem> _trayMenuItems = new();

    public MainWindowViewModel()
    {
        if (_isInitialized)
        {
            return;
        }

        InitializeViewModel();
    }

    private void InitializeViewModel()
    {
        ApplicationTitle = "WPF UI - ImapSync";

        NavigationItems.AddRange(new List<NavigationViewItem>()
        {
            new NavigationViewItem()
            {
                Content = "Home",
                Icon = new SymbolIcon { Symbol = SymbolRegular.Home24 },
                TargetPageType = typeof(Views.Pages.DashboardPage),
            },
            new NavigationViewItem()
            {
                Content = "Mail",
                Icon = new SymbolIcon { Symbol = SymbolRegular.Mail24 },
                TargetPageType = typeof(Views.Pages.MailPage),
            },
        });

        NavigationFooter.AddRange(new List<NavigationViewItem>()
        {
            new NavigationViewItem()
            {
                Content = "Settings",
                Icon = new SymbolIcon { Symbol = SymbolRegular.Settings24 },
                TargetPageType = typeof(Views.Pages.SettingsPage),
            },
        });

        TrayMenuItems.AddRange(new List<MenuItem>()
        {
            new MenuItem
            {
                Header = "Home",
                Tag = "tray_home",
            },
        });

        _isInitialized = true;
    }
}
