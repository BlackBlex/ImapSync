// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

using CommunityToolkit.Mvvm.Messaging;
using CommunityToolkit.Mvvm.Messaging.Messages;
using ImapSync.Models;
using ImapSync.Services;
using ImapSync.Views.Pages.Mail;
using Wpf.Ui;
using Wpf.Ui.Controls;

namespace ImapSync.ViewModels;

public partial class MailViewModel : ObservableObject, INavigationAware
{
    private readonly MailService _mailService;
    private readonly INavigationService _navigationService;

    private bool _isInitialized;

    [ObservableProperty]
    private IEnumerable<MailInfo> _mails;

    [ObservableProperty]
    private MailInfo _selectedMailInfo;

    public MailViewModel(MailService mailService, INavigationService navigationService)
    {
        _mailService = mailService;
        _navigationService = navigationService;
    }

    public void OnNavigatedTo()
    {
        if (_isInitialized)
        {
            return;
        }

        InitializeViewModel();
    }

    public void OnNavigatedFrom()
    {
    }

    private void InitializeViewModel()
    {
        _mailService.FetchMails();
        Mails = _mailService.GetMails();

        _mailService.BuildConnections();

        _isInitialized = true;
    }

    [RelayCommand]
    private void NavigateForward(Type type)
    {
        _ = _navigationService.NavigateWithHierarchy(type);

        if (type == typeof(ViewAccountPage))
        {
            _ = WeakReferenceMessenger.Default.Send(new ValueChangedMessage<MailInfo>(SelectedMailInfo));
        }
    }

    [RelayCommand]
    private void NavigateBack()
    {
        _ = _navigationService.GoBack();
    }
}
