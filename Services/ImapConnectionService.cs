// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

using System.Diagnostics;
using System.Net.Sockets;
using MailKit;
using MailKit.Net.Imap;

namespace ImapSync.Services;

public class ImapConnectionService
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Performance", "CA1822:Marcar miembros como static", Justification = "Dependency injection")]
    public (ImapConnection connection, TimeSpan time, string error) Build(string server, int port)
    {
        var watch = Stopwatch.StartNew();
        string error = String.Empty;

        var imap = new ImapConnection();

        try
        {
            imap.Connect(server, port);
        }
        catch (SocketException e)
        {
            error = e.Message;
        }

        watch.Stop();
        TimeSpan elapsedMs = watch.Elapsed;

        return (imap, elapsedMs, error);
    }
}

public class ImapConnection : IDisposable
{
    private readonly List<string> _folders = new();

    private ImapClient? _connection = null;

    private string _server = String.Empty;

    private string _mail = String.Empty;

    private int _port = -1;

    public void Connect(string server, int port)
    {
        _connection = new ImapClient();
        _connection.Connect(server, port, true);

        if (_connection.IsConnected)
        {
            _server = server;
            _port = port;
        }
    }

    public (bool isSucceed, TimeSpan time, string error) LoginTo(string mail, string passwd)
    {
        var watch = Stopwatch.StartNew();

        string error = String.Empty;
        try
        {
            _connection?.Authenticate(mail, passwd);
            _mail = mail;
        }
        catch (MailKit.Security.AuthenticationException e)
        {
            error = e.Message;
        }

        watch.Stop();
        TimeSpan elapsedMs = watch.Elapsed;

        return (_connection != null && _connection.IsAuthenticated, elapsedMs, error);
    }

    public (bool isSucceed, TimeSpan time, string error) ListFolders()
    {
        var watch = Stopwatch.StartNew();

        string error = String.Empty;

        try
        {
            _folders.Clear();
            if (_connection != null)
            {
                IList<IMailFolder> personal = _connection.GetFolders(_connection.PersonalNamespaces[0]);
                _folders.Add("INBOX");

                foreach (IMailFolder folder in personal)
                {
                    _folders.Add(folder.FullName);
                }
            }
            else
            {
                error = "Connection is null";
            }
        }
        catch (Exception e)
        {
            error = e.Message;
        }

        watch.Stop();
        TimeSpan elapsedMs = watch.Elapsed;

        return (_folders.Count != 0, elapsedMs, error);
    }

    public List<string> GetFolders() => _folders;

    public (IMailFolder? mailbox, TimeSpan time, string error) GetMailFolder(string folder)
    {
        var watch = Stopwatch.StartNew();

        string error = String.Empty;
        IMailFolder? mailbox = null;

        try
        {
            if (_connection != null)
            {
                mailbox = _connection.GetFolder(folder);
            }
            else
            {
                error = "Connection is null";
            }
        }
        catch (Exception e)
        {
            error = e.Message;
        }

        watch.Stop();
        TimeSpan elapsedMs = watch.Elapsed;

        return (mailbox, elapsedMs, error);
    }

    public void Dispose()
    {
        Dispose(true);
        GC.SuppressFinalize(this);
    }

    protected virtual void Dispose(bool disposing)
    {
        if (disposing)
        {
            _connection?.Disconnect(true);
            _connection?.Dispose();
        }
    }
}
