# **ImapSync | Respaldo de correos a archivos eml y PST**

---

Obtenga un respaldo via IMAP de los correos que administres, obten los archivos individuales (eml) y genere un PST (archivo de outlook)

---

Esta realizado con:

-   Lenguage: **CSharp**
-   Diseño: **XAML**
-   Frameworks: **WPF**, **.NET CORE**
-   Librerias: **WPF-UI**, **AutoGUI**, **CommunityToolkit.Mvvm**, **MailKit**, **Newtonsoft.Json**, **Outlook Redemption**

---

Características de la versión:

-   Carga archivos JSON con la estructura de Models/MailInfo.cs
-   Visualiza las carpetas del correo

---

## **Imágenes**

![Versión actual](images/main.png)
![Versión actual](images/view.png)

---

## Para ejecutar se debe:

1. Clonar el repositorio
2. Tener instalado [.NET Core 6.0](https://dotnet.microsoft.com/en-us/download) y [Visual Studio](https://visualstudio.microsoft.com/es/)
3. Abrir el proyecto con Visual Studio
4. Correr proyecto

---

ImapSync | Respaldo de correos a archivos eml y PST

Obtenga un respaldo via IMAP de los correos que administres, obten los archivos individuales (eml) y genere un PST (archivo de outlook)

Author: @BlackBlex (BlackBlex)

Licencia: **MIT**
