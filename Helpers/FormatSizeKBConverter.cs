// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

using System.Globalization;
using System.Text;
using System.Windows.Data;

namespace ImapSync.Helpers;

[ValueConversion(typeof(long), typeof(string))]
public sealed class FormatSizeKBConverter : IValueConverter
{
    public static readonly FormatSizeKBConverter Default = new();

    public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
    {
        long number = System.Convert.ToInt64(value);
        var sb = new StringBuilder(32);
        _ = NativeMethods.StrFormatByteSizeW(number, sb, sb.Capacity);
        return sb.ToString();
    }

    public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
    {
        return DependencyProperty.UnsetValue;
    }
}
