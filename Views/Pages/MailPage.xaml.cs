// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

using ImapSync.Views.Pages.Mail;
using Wpf.Ui.Controls;

namespace ImapSync.Views.Pages;

/// <summary>
/// Lógica de interacción para MailPage.xaml.
/// </summary>
public partial class MailPage : INavigableView<ViewModels.MailViewModel>
{
    public MailPage(ViewModels.MailViewModel viewModel)
    {
        ViewModel = viewModel;
        DataContext = this;

        InitializeComponent();
    }

    public ViewModels.MailViewModel ViewModel
    {
        get;
    }

    private void ListView_MouseDoubleClick(object sender, System.Windows.Input.MouseButtonEventArgs e)
    {
        ViewModel.NavigateForwardCommand.Execute(typeof(ViewAccountPage));
    }
}
