// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

using System.Diagnostics;
using System.Windows.Input;
using ImapSync.Helpers.Extensions;
using ImapSync.Models;
using Wpf.Ui.Controls;

namespace ImapSync.Views.Pages.Mail;

/// <summary>
/// Lógica de interacción para ViewAccountPage.xaml.
/// </summary>
public partial class ViewAccountPage : INavigableView<ViewModels.Mail.ViewAccountModel>
{
    public ViewAccountPage(ViewModels.Mail.ViewAccountModel viewModel)
    {
        ViewModel = viewModel;
        DataContext = viewModel;

        InitializeComponent();
    }

    public ViewModels.Mail.ViewAccountModel ViewModel
    {
        get;
    }

    private void OnItemMouseDoubleClick(object sender, MouseButtonEventArgs args)
    {
        if (sender is System.Windows.Controls.TreeViewItem tvi)
        {
            if (!tvi.IsSelected)
            {
                return;
            }

            if (treeViewImap.SelectedItem is ImapMailInfo imapInfo)
            {
                Debug.WriteLine(imapInfo.Name);
            }
        }
    }

    private void TreeViewImap_PreviewMouseRightButtonDown(object sender, MouseButtonEventArgs e)
    {
        System.Windows.Controls.TreeViewItem? treeViewItem = ((DependencyObject) e.OriginalSource).VisualUpwardSearch<System.Windows.Controls.TreeViewItem>();

        if (treeViewItem != null)
        {
            treeViewItem.IsSelected = true;
            e.Handled = true;
        }
    }
}
