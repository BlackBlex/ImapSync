// This Source Code Form is subject to the terms of the MIT License.
// If a copy of the MIT was not distributed with this file, You can obtain one at https://opensource.org/licenses/MIT.
// Copyright (C) BlackBlex and Contributors.
// All Rights Reserved.

using ImapSync.Services;

namespace ImapSync.Models;

public class MailInfo
{
    public string Host { get; set; } = String.Empty;

    public string User { get; set; } = String.Empty;

    public string Passwd { get; set; } = String.Empty;

    public int Port { get; set; } = 993;

    public ImapMailInfo ImapInfo { get; set; } = new ImapMailInfo();

    public ImapConnection ImapConnection { get; set; }
}
